#[allow(unused_imports)]
use std::env;
use std::fmt::Display;
use std::fmt::Error;
use std::fmt::Formatter;
use std::fs;
use std::io;
use std::io::Read;
use std::io::Write;
use std::path::Path;

use clap::command;
use clap::Parser;
use clap::Subcommand;
use flate2::read::ZlibDecoder;
use flate2::{write::ZlibEncoder, Compression};
use sha1::{Digest, Sha1};

#[derive(Debug, Parser)]
#[command(name = "git")]
#[command(about = "A CodeCrafters challenge Git CLI", long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Debug, Subcommand)]
enum Commands {
    Init,
    #[command(arg_required_else_help = true)]
    CatFile {
        #[arg(short = 'p')]
        print: bool,
        file: String,
    },
    #[command(arg_required_else_help = true)]
    HashObject {
        #[arg(short = 'w')]
        write: bool,
        file: String,
    },
    LsTree {
        #[arg(long)]
        name_only: bool,
        file: String,
    },
    CommitTree {
        tree: String,
        #[arg(short = 'p')]
        parent: String,
        #[arg(short = 'm')]
        message: String,
    },
    WriteTree,
}

#[derive(Debug)]
struct Tree {
    entries: Vec<TreeEntry>,
}

#[derive(Debug, PartialEq)]
enum TreeEntryMode {
    Directory,
    File,
}

type Sha = [u8; 20];

#[derive(Debug, PartialEq)]
struct TreeEntry {
    mode: TreeEntryMode,
    name: String,
    sha: Sha,
}

trait GitObject {
    fn to_bytes(&self) -> Vec<u8>;
}

impl GitObject for Tree {
    fn to_bytes(&self) -> Vec<u8> {
        let mut item_bytes = self.entries.iter().fold(Vec::new(), |mut out, e| {
            out.append(&mut e.to_bytes());
            out
        });
        let mut out = b"tree ".to_vec();
        out.append(&mut item_bytes.len().to_string().as_bytes().to_vec().to_owned());
        out.push(0);
        out.append(&mut item_bytes);
        out
    }
}

impl GitObject for TreeEntry {
    fn to_bytes(&self) -> Vec<u8> {
        let mut bytes = Vec::new();
        bytes.append(&mut self.mode.to_bytes());
        bytes.push(b' ');
        bytes.append(&mut self.name.as_bytes().to_vec());
        bytes.push(0);
        bytes.append(&mut self.sha.to_vec());
        bytes
    }
}

impl Display for TreeEntryMode {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        match &self {
            TreeEntryMode::Directory => write!(f, "040000 tree"),
            TreeEntryMode::File => write!(f, "100644 blob"),
        }
    }
}

impl GitObject for TreeEntryMode {
    fn to_bytes(&self) -> Vec<u8> {
        match &self {
            TreeEntryMode::Directory => b"40000".to_vec(),
            TreeEntryMode::File => b"100644".to_vec(),
        }
        .to_vec()
    }
}

fn main() {
    // You can use print statements as follows for debugging, they'll be visible when running tests.
    eprintln!("Logs from your program will appear here!");

    let args = Cli::parse();
    match args.command {
        Commands::Init => {
            // create directory & file structure
            fs::create_dir(".git").unwrap();
            fs::create_dir(".git/objects").unwrap();
            fs::create_dir(".git/refs").unwrap();
            fs::write(".git/HEAD", "ref: refs/heads/main\n").unwrap();
            println!("Initialized git directory")
        }
        Commands::CatFile { print: _, file } => {
            // find file based on hash
            let path = hash_filepath(file);
            let content = fs::read(path).expect("file should exist");

            // decode
            let mut decoder = ZlibDecoder::new(content.as_slice());
            let mut decoded = String::new();
            decoder
                .read_to_string(&mut decoded)
                .expect("should be valid UTF-8");

            // process based on type
            match decoded
                .split_once(' ')
                .expect("should have type followed by content")
            {
                ("blob", rest) => {
                    if let Some((size, data)) = rest.split_once('\0') {
                        print!("{}", &data[..size.parse::<usize>().unwrap()]);
                    }
                }
                _ => {
                    eprintln!("unknown file object type");
                }
            }
        }
        Commands::HashObject { write, file } => {
            let hash = hash_object(Path::new(&file), write);
            println!("{}", hex::encode(hash));
        }
        Commands::LsTree { name_only, file } => {
            // fetch the content
            let path = hash_filepath(file);
            let content = fs::read(path).expect("file should exist");

            // decode
            let mut decoder = ZlibDecoder::new(content.as_slice());
            let mut decoded = Vec::new();
            decoder
                .read_to_end(&mut decoded)
                .expect("should be valid UTF-8");

            // gather header
            let mut index = 0;
            while decoded[index] != 0 {
                index += 1;
            }
            let header = &decoded[..index];
            if !header.starts_with("tree".as_bytes()) {
                panic!("object must be a tree");
            }

            // get tree items & display
            let items = unfold(parse_tree_item, &decoded[index + 1..]);
            for i in items {
                if !name_only {
                    print!("{} {} ", i.mode, hex::encode(i.sha));
                }
                println!("{}", i.name);
            }
        }
        Commands::CommitTree {
            tree,
            parent,
            message,
        } => {
            let object = format!(
                "commit 123\0\
                 tree {tree}\n\
                 parent {parent}\n\
                 author Si Nicholls <si@mintsource.org> 1715242803 +0100\n\
                 committer Si Nicholls <si@mintsource.org> 1715242803 +0100\n\
                 \n\
                 {message}\n"
            );
            let mut hasher = Sha1::new();
            hasher.update(&object.as_bytes());
            let sha = hasher.finalize().into();
            write_object(&sha, &object.as_bytes());
            println!("{}", hex::encode(sha))
            // println!("{}", hex::encode(write_tree(Path::new("."))));
        }
        Commands::WriteTree => {
            println!("{}", hex::encode(write_tree(Path::new("."))));
        }
    }
}

fn hash_object(path: &Path, write: bool) -> Sha {
    // pull content together
    let mut content = fs::read(path).expect("file should exist");
    let mut object = Vec::from(b"blob ");
    object.append(&mut content.len().to_string().into_bytes());
    object.push(0);
    object.append(&mut content);

    // hash it
    let mut hasher = Sha1::new();
    hasher.update(&object);
    let sha = hasher.finalize().into();

    if write {
        write_object(&sha, &object);
    }

    sha
}

fn write_object(sha: &Sha, content: &[u8]) {
    // encode content
    let mut encoder = ZlibEncoder::new(Vec::new(), Compression::default());
    encoder.write_all(&content).expect("should be valid UTF-8");
    let result = encoder.finish().unwrap();

    // write it
    let hash = hex::encode(sha);
    let (dir, filename) = hash_path_and_file(hash);
    fs::create_dir_all(&dir).unwrap();
    fs::write(format!("{}/{}", &dir, &filename), &result).unwrap();

    // eprintln!("write_object {}: {:?}", &hash, &result);
}

fn write_tree(path: &Path) -> Sha {
    // get name ordered entry list
    let mut dir_entries = fs::read_dir(path)
        .expect("unable to read dir")
        .collect::<Result<Vec<_>, io::Error>>()
        .expect("couldn't collect directory entries");
    dir_entries.sort_by_cached_key(|e| e.file_name());

    // recursively build tree entries
    let mut entries = Vec::new();
    for file in dir_entries {
        if file.file_name() == ".git" {
            continue;
        }

        let path = file.path();
        if path.is_dir() {
            // recurse for directory
            entries.push(TreeEntry {
                mode: TreeEntryMode::Directory,
                name: file.file_name().into_string().unwrap(),
                sha: write_tree(&path),
            });
        } else if path.is_file() {
            // else add file
            entries.push(TreeEntry {
                mode: TreeEntryMode::File,
                name: file.file_name().into_string().unwrap(),
                sha: hash_object(&path, true),
            });
        }
    }

    // build tree & hash it
    let tree = Tree { entries };
    let object = tree.to_bytes();
    let mut hasher = Sha1::new();
    hasher.update(&object);
    let sha = hasher.finalize().into();

    write_object(&sha, &object);
    sha
}

fn hash_path_and_file(hash: String) -> (String, String) {
    let (dir, filename) = hash.split_at(2);
    let dir = format!(".git/objects/{}", &dir);
    (dir, filename.to_owned())
}

fn hash_filepath(hash: String) -> String {
    let (dir, filename) = hash_path_and_file(hash);
    format!("{}/{}", &dir, &filename)
}

fn unfold<F, T, U>(func: F, seed: U) -> Vec<T>
where
    F: Fn(U) -> Option<(T, U)>,
{
    let mut seed = seed;
    let mut acc = Vec::new();
    loop {
        match func(seed) {
            Some((t, u)) => {
                acc.push(t);
                seed = u;
            }
            None => break,
        }
    }
    acc
}

fn parse_tree_item(bs: &[u8]) -> Option<(TreeEntry, &[u8])> {
    if bs.is_empty() {
        return None;
    };

    let mut index = 0;
    let start = index;
    while bs[index] != b' ' {
        index += 1;
    }
    let mode = match &bs[start..index] {
        b"40000" | b"040000" => TreeEntryMode::Directory,
        b"100644" => TreeEntryMode::File,
        _ => panic!("invalid tree entry mode"),
    };

    // name, skipping space
    index += 1;
    let start = index;
    while bs[index] != 0 {
        index += 1;
    }
    let name = String::from_utf8_lossy(&bs[start..index]).into_owned();

    // sha, skipping NULL
    let sha = &bs[index + 1..index + 21];
    Some((
        TreeEntry {
            mode,
            name,
            sha: sha.try_into().unwrap(),
        },
        &bs[index + 21..],
    ))
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_fold<'a>() {
        let f = |bs: &'a str| -> Option<(&'a str, &'a str)> {
            if let Some(i) = bs.find(" ") {
                Some((&bs[..i], &bs[i + 1..]))
            } else if !bs.is_empty() {
                Some((&bs[..], Default::default()))
            } else {
                None
            }
        };
        assert_eq!(unfold(f, "pizza is tasty"), vec!["pizza", "is", "tasty"])
    }

    #[test]
    fn test_parse_tree_item() {
        let bs = b"40000 adirectory\056789012345678901234\
                               100644 beers.txt\001234567890123456789\
                               100644 pizza.txt\098765432109876543210";
        assert_eq!(
            unfold(parse_tree_item, bs),
            vec![
                TreeEntry {
                    mode: TreeEntryMode::Directory,
                    name: String::from("adirectory"),
                    sha: *b"56789012345678901234"
                },
                TreeEntry {
                    mode: TreeEntryMode::File,
                    name: String::from("beers.txt"),
                    sha: *b"01234567890123456789"
                },
                TreeEntry {
                    mode: TreeEntryMode::File,
                    name: String::from("pizza.txt"),
                    sha: *b"98765432109876543210"
                },
            ]
        )
    }
}
